#include "gd32f1x0.h"

#define DELAY_TIMER TIMER5
#define DELAY_TIMER_RCU RCU_TIMER5

void delay_config_us(void);
void delay_config_iic(void);
void delay_us(uint16_t us);
void delay_iic(void);
void delay_iic_hight(void);
