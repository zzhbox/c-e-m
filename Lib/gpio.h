#include "gd32f1x0_gpio.h"

typedef struct
{
    rcu_periph_enum rcu_periph;
    uint32_t gpio_periph;
    uint32_t pin;
}gpio_struct;

/* GPIO pin enum */
typedef enum
{
    GA0,
    GA1,
    GA2,
    GA3,
    GA4,
    GA5,
    GA6,
    GA7,
    GA8,
    GA9,
    GA10,
    GA11,
    GA12,
    GA13,
    GA14,
    GA15,
    GAALL,

    GB0,
    GB1,
    GB2,
    GB3,
    GB4,
    GB5,
    GB6,
    GB7,
    GB8,
    GB9,
    GB10,
    GB11,
    GB12,
    GB13,
    GB14,
    GB15,
    GBALL,

    GC0,
    GC1,
    GC2,
    GC3,
    GC4,
    GC5,
    GC6,
    GC7,
    GC8,
    GC9,
    GC10,
    GC11,
    GC12,
    GC13,
    GC14,
    GC15,
    GCALL,
    
    GD0,
    GD1,
    GD2,
    GD3,
    GD4,
    GD5,
    GD6,
    GD7,
    GD8,
    GD9,
    GD10,
    GD11,
    GD12,
    GD13,
    GD14,
    GD15,
    GDALL,

    GF0,
    GF1,
    GF2,
    GF3,
    GF4,
    GF5,
    GF6,
    GF7,
    GF8,
    GF9,
    GF10,
    GF11,
    GF12,
    GF13,
    GF14,
    GF15,
    GFALL,
}gpio_enum;

void gpio_enable_pull(gpio_enum e_gpio, uint32_t mode, uint32_t pull_up_down);
void gpio_enable(gpio_enum e_gpio, uint32_t mode);
void gpio_enable_AF(gpio_enum e_gpio, uint32_t alt_func_num);

void gpio_write(gpio_enum e_gpio, uint8_t val);
uint8_t gpio_read(gpio_enum e_gpio);
